from django.shortcuts import render
from django.contrib.auth.models import User
from ProjectApp.models import register_recruiter,register_candidate,post_job,addQuestion,apply,contact
from django.contrib.auth import authenticate,login,logout
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import check_password
from django.core.mail import EmailMessage
import datetime
import random 

# Create your views here.

def home(request):
    all=post_job.objects.all()[:6]
    if request.method=="POST":
        to_search = request.POST['content']
        all = post_job.objects.filter(position__contains=to_search)|post_job.objects.filter(city__contains=to_search)
        t=''
        if len(all)==0:
            t='No Results for "{}"'.format(to_search)
        else:
            t='Total {} results for "{}"'.format(len(all),to_search)

        return render(request,'home.html',{'all':all,'total':t})

    if 'username' in request.COOKIES:
        un=request.COOKIES['username']
        user_obj=User.objects.get(username=un)
        login(request,user_obj)
        if un.is_staff:
            return HttpResponseRedirect('/ProjectApp/recruiter_dash')
        else:
            return HttpResponseRedirect('/ProjectApp/candidate_dash')

    return render(request,'home.html',{'all':all})

def footer(request):
    return render(request,'footer.html')

def signRecruiter(request):
    if request.method=='POST':
        print(request.POST)
        un=request.POST['user']
        cn=request.POST['company']
        pic=request.FILES['image']
        em=request.POST['email']
        pwd=request.POST['password']
        con=request.POST['contact']
        city=request.POST['city']
        add=request.POST['address']
        # save data in authentication 
        data=User.objects.create_user(username=un,email=em,password=pwd)
        data.is_staff=True
        # data.set_password(data.password)
        data.save()

        data1=register_recruiter(user=data,company=cn,profile_pic=pic,contact=con,city=city,address=add)
        data1.save()
        return HttpResponseRedirect('/ProjectApp/login?msz=Sign Up Successfully,Login Now')

    return render(request,'signRecruiter.html')

def signCandidate(request):
    if request.method=='POST':
        fn=request.POST['first_name']
        ln=request.POST['last_name']
        pic=request.FILES['image']
        un=request.POST['user']
        em=request.POST['email']
        pwd=request.POST['pw']
        con=request.POST['contact']
        date=request.POST['dob']
        gen=request.POST['gender']
        city=request.POST['city']
        add=request.POST['address']

        data=User.objects.create_user(username=un,email=em,password=pwd)
        # data.set_password(data.password)
        data.save()

        data1=register_candidate(user=data,first=fn,last=ln,profile_pic=pic,contact=con,dob=date,gender=gen,city=city,address=add)
        data1.save()
        return HttpResponseRedirect('/ProjectApp/login?msz=Sign Up Successfully,Login Now')

    return render(request,'signCandidate.html')

@login_required
def recruiter_dash(request):
    log=User.objects.get(username=request.user)
    rec=register_recruiter.objects.get(user=log)
    return render(request,'rec_first.html',{'profile':rec})

def candidate_dash(request):
    log=User.objects.get(username=request.user.username)
    can=register_candidate.objects.get(user=log)
    if request.method=="POST":
        
        if 'place' in request.POST:
            pl = request.POST['place']
            all = post_job.objects.filter(city__contains=pl)
            return render(request,'all_jobs.html',{'profile':can,'all':all,'total':len(all)})

        if 'pos' in request.POST:
            po = request.POST['pos']        
            all = post_job.objects.filter(position__contains=pos)
            return render(request,'all_jobs.html',{'profile':can,'all':all,'total':len(all)})

    return render(request,'cand_first.html',{'profile':can})

def uslogin(request):
    msz=''
    if 'msz' in request.GET:
        msz = request.GET['msz']
    if request.method=='POST':
        username = request.POST['user']
        password = request.POST['password']

        user = authenticate(username=username,password=password)
        if user:
            if user.is_staff:
                login(request,user)
                response = HttpResponseRedirect('/ProjectApp/recruiter_dash')
                if 'rememberme' in request.POST:
                    response.set_cookie('username',username)
                    response.set_cookie('userid',user.id)
                    response.set_cookie('logintime',datetime.datetime.now())
                return response
            if user.is_active:
                # return HttpResponse('Done')
                login(request,user)
                response = HttpResponseRedirect('/ProjectApp/candidate_dash')
                if 'rememberme' in request.POST:
                    response.set_cookie('username',username)
                    response.set_cookie('userid',user.id)
                    response.set_cookie('logintime',datetime.datetime.now())
                return response
            else:
                return render(request,'home.html',{'status':'Sorry! You are not registered user!!!'})

        return render(request,'home.html',{'status':'Sorry You are not registered User!!!'})
    return render(request,'login.html',{'status':msz})

@login_required
def uslogout(request):
    logout(request)
    response = HttpResponseRedirect('/')
    response.delete_cookie('username')
    response.delete_cookie('logintime')
    response.delete_cookie('id')
    return response 

def forgot_password(request):
    if request.method=='POST':
        name=request.POST['usname']
        pwd=request.POST['password']
        user_obj=User.objects.get(username=name)
        user_obj.set_password(pwd)
        user_obj.save()
        return render(request,'login.html',{'status':'Password Set, Login Now!!'})

    return render(request,'forgot.html')

def about(request):
    return render(request,'About.html')

def contactus(request):
    if request.method=='POST':
        nm=request.POST['un']
        cont=request.POST['con']
        eml=request.POST['email']
        sub=request.POST['sub']
        msz=request.POST['msg']

        data=contact(name=nm,phone_no=cont,email=eml,subject=sub,message=msz)
        data.save()
        return render(request,'contact.html',{'status':'Success!'})

    return render(request,'contact.html')

def checkuser(request):
    if request.method=='GET':
        un=request.GET['usern']
        ch=User.objects.filter(username=un)
        if len(ch)>=1:
            return HttpResponse('User with this name already exists!!')
        else:
            return HttpResponse('Username Validation Success!!')
        print(ch,len(ch))

def find_user(request):
    if request.method=='GET':
        un=request.GET['usern']
        ch=User.objects.filter(username=un)
        if len(ch)==0:
            return HttpResponse('not')
        else:
            user = User.objects.get(username=un)
            to=user.email
            sub='Password Retrieval'
            rn=random.randint(100000,999999)
            msg='Your OTP to setup new password is '+str(rn)
            e=EmailMessage(sub,msg,to=[to,])
            e.send()
            return HttpResponse(user.email+','+str(rn))

@login_required
def change_password(request):
    log=User.objects.get(username=request.user)
    rec=register_recruiter.objects.get(user=log)
    if request.method=='POST':
        oldp=request.POST['old']
        np=request.POST['new']
        match=check_password(oldp,user_obj.password)
        if match==True:
            user_obj.set_password(np)
            user_obj.save()
            return render(request,'change_password.html',{'profile':rec,'status':'Password Changed Successfully!!','col':'success'})
        else:
            return render(request,'change_password.html',{'profile':rec,'status1':'Incorrect Current Password!!','col':'danger'})

    return render(request,'change_password.html',{'profile':rec})

@login_required
def cand_chng_pass(request):
    log=User.objects.get(username=request.user.username)
    can=register_candidate.objects.get(user=log)
    if request.method=='POST':
        old=request.POST['old']
        np=request.POST['new']
        match=check_password(old,log.password)
        if match==True:
            log.set_password(np)
            log.save()
            return render(request,'cand_chng_pass.html',{'profile':can,'status':'Password Changed Successfully!!','col':'success'})
        else:
            return render(request,'cand_chng_pass.html',{'profile':can,'status1':'Incorrect Current Password!!','col':'danger'})

    return render(request,'cand_chng_pass.html',{'profile':can,})

@login_required
def change_profile(request):
    log=User.objects.get(username=request.user.username)
    rec=register_recruiter.objects.get(user=log)
    if request.method=='POST':
        username=request.POST['username']
        company=request.POST['company']
        email=request.POST['email']
        con=request.POST['contact']
        city=request.POST['city']
        address=request.POST['address']

        if 'image' in request.FILES:
            pic=request.FILES['image']
            rec.profile_pic=pic
            rec.save()

        rec.username=username
        rec.email=email
        rec.company=company
        rec.contact=con   
        rec.city=city
        rec.address=address        
        rec.save()
        return render(request,'change_profile.html',{'reg':rec,'profile':rec,'status':'Changed Successfully!!'})

    return render(request,'change_profile.html',{'reg':rec,'profile':rec})

@login_required
def cand_chng_profile(request):
    log=User.objects.get(username=request.user.username)
    can=register_candidate.objects.get(user=log)
    if request.method=='POST':
        fn=request.POST['first_name']
        ln=request.POST['last_name']
        username=request.POST['username']
        em=request.POST['email']
        con=request.POST['contact']
        dob=request.POST['dob']
        city=request.POST['city']
        addr=request.POST['address']

        if 'image' in request.FILES:
            pic=request.FILES['image']
            can.profile_pic=pic
            can.save()

        can.first=fn
        can.last=ln
        can.username=username
        can.email=em
        can.contact=con        
        can.dob=dob
        can.city=city
        can.address=addr   
        can.save()
        return render(request,'cand_chng_profile.html',{'profile':can,'status':'Changed Successfully!!'})

    return render(request,'cand_chng_profile.html',{'profile':can})

@login_required
def view_profile(request):
    log=User.objects.get(username=request.user)
    rec=register_recruiter.objects.get(user=log)
    return render(request,'view_profile.html',{'profile':rec})

@login_required
def candidate_profile(request):
    log=User.objects.get(username=request.user)
    can=register_candidate.objects.get(user=log)
    return render(request,'candidate_profile.html',{'profile':can})

@login_required
def postjob(request):
    rec = register_recruiter.objects.get(user__username=request.user.username)
    if request.method=="POST":
        pos=request.POST['position']
        sal=request.POST['sal']
        vac=request.POST['vacancy']
        reqs=request.POST['req']
        hrname=request.POST['hrname']
        hrcon=request.POST['hrcontact']
        city=request.POST['city']
        exp=request.POST['exp']
        des=request.POST['desc']

        post_obj = post_job(name=rec,position=pos,salary=sal,vacancies=vac,requirements=reqs,hr_name=hrname,hr_contact=hrcon,city=city,experience=exp,description=des)
        post_obj.save()
        return render(request,'post_job.html',{'profile':rec,'status':'Posted Successfully!!',})
    return render(request,'post_job.html',{'profile':rec,})

@login_required
def mypost(request):
    rec = register_recruiter.objects.get(user__username=request.user.username)
    all = post_job.objects.filter(name=rec)
    if len(all)==0:
        return render(request,'view_jobs.html',{'profile':rec,'status':'No Posts Yet'})
    else:
        return render(request,'view_jobs.html',{'profile':rec,'all':all})

def desc_job(request):
    rec = register_recruiter.objects.get(user__username=request.user.username)
    if 'id' in request.GET:
        id_to_show=request.GET['id']
        p=post_job.objects.get(id=id_to_show)
        return render(request,'post_description.html',{'profile':rec,'ds':p,})

@login_required
def cand_desc_job(request):
    log=User.objects.get(username=request.user)
    can=register_candidate.objects.get(user=log)
    if 'id' in request.GET:
        id_to_show=request.GET['id']
        p=post_job.objects.get(id=id_to_show)      
        return render(request,'cand_desc_job.html',{'profile':can,'ds':p})

@login_required
def cand_first(request):
    can=register_candidate.objects.get(user__username=request.user.username)    
    all=post_job.objects.all().order_by('-id')
    if request.method=="POST":
        
        if 'place' in request.POST:
            pl = request.POST['place']
            all = post_job.objects.filter(city__contains=pl)
            return render(request,'all_jobs.html',{'profile':can,'all':all,'total':len(all)})

        if 'pos' in request.POST:
            po = request.POST['pos']        
            all = post_job.objects.filter(position__contains=pos)
            return render(request,'all_jobs.html',{'profile':can,'all':all,'total':len(all)})

    return render(request,'cand_first.html',{'profile':can,})

@login_required
def rec_first(request):
    return render(request,'rec_first.html')

@login_required
def recent_posts(request):
    log=User.objects.get(username=request.user)
    can=register_candidate.objects.get(user=log)
    all_jobs = post_job.objects.all().order_by('-id')
    recent = all_jobs[:6]
    if 'all' in request.GET:
        return render(request,'recent_posts.html',{'profile':can,'all':all_jobs,'type':'all'})
    return render(request,'recent_posts.html',{'profile':can,'all':recent,})

@login_required
def edit_post(request):
    rec = register_recruiter.objects.get(user__username=request.user.username)
    jobid=request.GET['id']
    if 'id' in request.GET:
        id_to_edit=request.GET['id']
        p = post_job.objects.get(id=id_to_edit,name=rec)

        if request.method=='POST':
            pos=request.POST['position']
            sal=request.POST['sal']
            vac=request.POST['vacancy']
            reqs=request.POST['req']
            hrname=request.POST['hrname']
            hrcon=request.POST['hrcontact']
            city=request.POST['city']
            exp=request.POST['exp']
            des=request.POST['desc']
            p.position=pos
            p.salary=sal    
            p.vacancies=vac
            p.requirements=reqs
            p.hr_name=hrname
            p.hr_contact=hrcon
            p.city=city
            p.experience=exp
            p.description=des
            p.save() 
            return render(request,'edit_post.html',{'profile':rec,'post':p,'status':'Edited Successfully!!'})
        
    return render(request,'edit_post.html',{'profile':rec,'post':p})

@login_required
def delete_post(request):
    rec = register_recruiter.objects.get(user__username=request.user.username)
    jobid=request.GET['id']
    if 'id' in request.GET:
        id_to_delete=request.GET['id']
        p = post_job.objects.get(id=id_to_delete,name=rec)
        p.delete()
        return HttpResponseRedirect('/ProjectApp/posted_jobs')

def quiz(request):
    log=User.objects.get(username=request.user)
    can=register_candidate.objects.get(user=log)
    postid = request.GET.get('id')
    companyid = request.GET.get('cmid')
    quiz = addQuestion.objects.all()
    no_of_questions=len(quiz)
    right=0
    wrong=0
    if request.method == 'POST':
        for question in quiz:
            if(request.POST['q'+repr(question.id)] == request.POST['a'+repr(question.id)]):
                right +=1
            else:
                wrong +=1
        total_score =  no_of_questions*2
        secured = right * 2
        percentage = (secured/total_score)*100
        st=''
        if percentage >= 75:
            st = "Congratulations you cracked It !!!"
            img = '/static/images/good.gif'
            return render(request,'quiz_result.html',{'profile':can,'p':postid,'c':companyid,'img':img,'st':st,'tq':no_of_questions,'total':total_score,'secured':secured,'percentage':percentage,'rs':'Pass','ap':'ap'})
        else:
            img = '/static/images/tenor.gif'
            st = "Sorry!!! You are not eligible for this job"
            return render(request,'quiz_result.html',{'profile':can,'img':img,'st':st,'tq':no_of_questions,'total':total_score,'secured':secured,'percentage':percentage,'rs':'Fail'})

    return render(request,'quiz.html',{'profile':can,'que':quiz})

@login_required
def apply_job(request):
    can=register_candidate.objects.get(user__username=request.user.username)    
    jobid=request.GET['id']
    cmpid=request.GET['cmid']
    pj=post_job.objects.get(id=jobid)
    rec=register_recruiter.objects.get(id=cmpid)
    if request.method=='POST':
        ql=request.POST['qual']
        pr=request.POST['per']
        pyr=request.POST['year']
        rm=request.FILES['res']
        epr=request.POST['exp']

        data=apply(candidate=can,job=pj,recruiterid=rec,qualification=ql,graduation_percentage=pr,passing_out_year=pyr,resume=rm,experience=epr)
        data.save()
        if 'rm' in request.FILES:
            img=request.FILES['res']
            data.save()
        to=request.user.email
        sub='Job Application Through Campus Recruitment'
        msg='''
        Dear {},
        You have successfully applied for {} in {}.
        You will receive a response from the recruiter as per their workarea.
        Thanks for opting Recruitment System for your requisite.
        Regards,
        Team Recruitment System 
        '''.format(request.user.username,pj.position,rec.company)
        e=EmailMessage(sub,msg,to=[to,])
        e.send()
        return render(request,'apply_form.html',{'profile':can,'status':'Applied Successfully!!'})

    return render(request,'apply_form.html',{'profile':can})

@login_required
def applicants(request):
    rec = register_recruiter.objects.get(user__username=request.user.username)
    all=apply.objects.filter(recruiterid__user__username=request.user.username).order_by('-id')
    if len(all)==0:
        return render(request,'applicants.html',{'profile':rec,'status':'No Candidate Applied'})
    else:
        return render(request,'applicants.html',{'profile':rec,'all':all})

def all_jobs(request):
    can=register_candidate.objects.get(user__username=request.user.username)    
    all_jobs = post_job.objects.all()[:6]
    return render(request,'home.html',{'profile':can,'all':all_jobs})

@login_required
def applied_for(request):
    can=register_candidate.objects.get(user__username=request.user.username)    
    all=apply.objects.filter(candidate__user__username=request.user.username).order_by('-id')
    if len(all)==0:
        return render(request,'applied_for.html',{'profile':can,'status':'You Have not applied for any job yet.'})
    else:
        return render(request,'applied_for.html',{'profile':can,'all':all})   

@login_required
def mail(request):
    cnid=request.GET['id']
    rec = register_recruiter.objects.get(user__username=request.user.username)
    cand = register_candidate.objects.get(id=cnid)
    if request.method=='POST':
        to=request.POST['app']
        sub=request.POST['subj']
        msg=request.POST['message']
        e=EmailMessage(sub,msg,to=[to,])
        e.send()
        return render(request,'mail.html',{'profile':rec,'can':cand,'status':'Email Sent Successfully!!'})         

    return render(request,'mail.html',{'profile':rec,'can':cand})     