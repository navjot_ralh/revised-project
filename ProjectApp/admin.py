from django.contrib import admin
from ProjectApp.models import register_recruiter,register_candidate,post_job,addQuestion,apply,contact
# Register your models here.
admin.site.register(register_recruiter)
admin.site.register(register_candidate)
admin.site.register(post_job)
admin.site.register(addQuestion)
admin.site.register(apply)
admin.site.register(contact)